<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/singleton.php";

    class Settings{


        //{"1":"Синий","2":"Красный","3":"Зеленый"}

        /**
         * Получение настроек заявки
         *
         * @param integer $id - id строки настроек в базе
         * @return array - одномерный массив настроек заявки
         */
        public static function select_setting($id){
            $res = DBQuery::query("SELECT * FROM `settings`where `id` = $id");
            $res[0][8] = json_decode($res[0][8],true);
            if (!$res){
                $res = false;
            }
            return $res[0];
        }

        /**
         * Кодирование JSON цветов в массив
         *
         * @param  JSON $colors_json - JSON цветов
         * @return array - массив цветов
         */
        public static function decode_colors($colors_json){
            return json_decode($colors_json,true);
        }

        /**
         * Кодирование массива цветов в JSON
         *
         * @param array $colors_array - массив цветов
         * @return string - JSON цветов
         */
        public static function encode_colors($colors_array){
            return json_encode($colors_array,JSON_UNESCAPED_UNICODE);
        }

        /**
         * Получение доступных цветов потолка в виде массива
         *
         * @param integer $id - id строки настроек
         * @return array - массив цветов
         */
        public static function get_colors_array($id){
            $res = DBQuery::query("SELECT * FROM `settings` WHERE `id` = $id");
            $res[0][8] = json_decode($res[0][8],true);
            if (!$res){
                $res = false;
            }
            return $res[0][8];
        }

        /**
         * Добавление нового цвета в список цветов потолков в базе
         *
         * @param string $new_color - новый цвет
         * @return mixed выполнение запроса UPDATE к базе
         */
        public static function add_color($new_color){
            if(!empty($new_color)){
                $colors_array = Settings::get_colors_array(1);
                $key = array_key_last($colors_array) + 1;
                $colors_array[$key] = $new_color;
                $colors_json = Settings::encode_colors($colors_array);
            return DBQuery::query("UPDATE `settings` SET `colors` = '$colors_json' WHERE `id` = 1");
            } return false;
        }

        /**
         * Удаление цвета из списка цветов потолков в базе
         *
         * @param integer $color_id - id цвета, который нужно удалить
         * @return mixed - выполнение запроса DELETE к базе
         */
        public static function delete_color($color_id){
            $colors_array = Settings::get_colors_array(1);
            unset($colors_array[$color_id]);
            $colors_json = Settings::encode_colors($colors_array);
            return DBQuery::query("UPDATE `settings` SET `colors` = '$colors_json' WHERE `id` = 1");

        }


        /**
         * Обновление настроек заявки
         *
         * @param integer $square_meter_price - цена за кв метр потолка
         * @param integer $lamp_price - цена за светильник
         * @param integer $chandelier_price - цена за люстру
         * @param integer $aqueduct_price - цена за трубу
         * @param integer $glance_price - цена за глянцевое покрытие
         * @param integer $matte_price - цена за матовое покрытие
         * @param string $new_color - новый цвет потолков
         * @return mixed выполнение запроса UPDATE к базе
         */
        public static function update_setting($square_meter_price, $lamp_price, $chandelier_price, $aqueduct_price, $corner_price, $glance_price, $matte_price,$new_color){
            Settings::add_color($new_color);

            return DBQuery::query("UPDATE `settings` 
                                    SET `square_meter_price` = $square_meter_price, 
                                        `lamp_price` = $lamp_price, 
                                        `chandelier_price` = $chandelier_price,
                                        `aqueduct_price` = $aqueduct_price,
                                        `corner_price` = $corner_price,
                                        `glance_price` = $glance_price,
                                        `matte_price` = $matte_price
                                    WHERE `id` = 1");
        }



    }
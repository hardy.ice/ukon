<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/singleton.php";

    class Requests{

        /**
         * Получение всех заявок
         *
         * @return array - массив всех заявок
         */
        public static function select_all_requests(){
            $res = DBQuery::query("SELECT id, phone, birth_date, delivery_city, text, request_date, user_ip FROM requests");
            if (!$res){
                $res = false;
            }
            return $res;
        }

        /**
         * Создание текста заявки
         *
         * @param integer $square - площадь потолка, введенная пользователем в форму создания заявки
         * @param integer $lamp - количество светильников, введенное пользователем в форму создания заявки
         * @param integer $chandelier - количество люстр, введенное пользователем в форму создания заявки
         * @param integer $aqueduct - количество труб, введенное пользователем в форму создания заявки
         * @param integer $corner - количество углов, введенное пользователем в форму создания заявки
         * @param string $texture - фарктура, выбранная пользователем в форме создания заявки
         * @param string $color - цвет потолка, выбранный пользователем в форме создания заявки
         * @param integer $total_price - итоговая цена, рассчитанная системой на основе параметров, указанных польхзователем в форме создания заявки
         * @return string - текст заявки
         */
        public static function make_request_text($square, $lamp, $chandelier, $aqueduct, $corner, $texture, $color, $total_price){
            $str = "";
            $str .= "площадь потолка: " . $square;
            $str .= ";  количество светильников: " . $lamp;
            $str .= ";  количество люстр: " . $chandelier;
            $str .= ";  количество труб: " . $aqueduct;
            $str .= ";  количество углов: " . $corner;
            $str .= ";  фактура: " . "$texture";
            $str .= ";  цвет: " . "$color";
            $str .= ";  ИТОГО: " . "$total_price";
            return $str;
        }

        /**
         * Создание заявки
         *
         * @param string $delivery_city - город доставки, введенный пользователем в форму создания заявки
         * @param date $birth_date - дата рождения, введенная пользователем в форму создания заявки
         * @param string $phone - номер телефона, введенный пользователем в форму создания заявки
         * @param integer $square - площадь потолка, введенная пользователем в форму создания заявки
         * @param integer $lamp - количество светильников, введенное пользователем в форму создания заявки
         * @param integer $chandelier - количество люстр, введенное пользователем в форму создания заявки
         * @param integer $aqueduct - количество труб, введенное пользователем в форму создания заявки
         * @param integer $corner - количество углов, введенное пользователем в форму создания заявки
         * @param string $texture - фарктура, выбранная пользователем в форме создания заявки
         * @param string $color - цвет потолка, выбранный пользователем в форме создания заявки
         * @param integer $total_price - итоговая цена, рассчитанная системой на основе параметров, указанных польхзователем в форме создания заявки
         * @param datetime $request_date - дата создания заявки
         * @param string $user_ip - ip пользователя, создавшего заявку
         * @return mixed - выполнение запроса INSERT к базе
         */
        public static function add_request($delivery_city, $birth_date, $phone, $square, $lamp, $chandelier, $aqueduct, $corner, $texture, $color, $total_price, $request_date, $user_ip){
            $request_text = Requests::make_request_text($square,
                                                        $lamp,
                                                        $chandelier,
                                                        $aqueduct,
                                                        $corner,
                                                        $texture,
                                                        $color,
                                                        $total_price);
            return DBQuery::query("INSERT INTO requests (`delivery_city`, 
                                                        `birth_date`, 
                                                        `phone`, 
                                                        `text`, 
                                                        `request_date`, 
                                                        `user_ip`) 
                                    VALUES ('$delivery_city', 
                                            '$birth_date', 
                                            '$phone', 
                                            '$request_text',
                                            '$request_date', 
                                            '$user_ip')");
        }

        /**
         * Загрузка файла, прикрепленного к заявке, на сервер и перемещение его в папку mail
         */
        public static function upload_file(){

            $upload_dir = $_SERVER["DOCUMENT_ROOT"].'/mail';
            if( ! is_dir( $upload_dir ) ) mkdir( $upload_dir, 0777 );

            $files      = $_FILES; // полученные файлы
            $done_files = array();

            foreach( $files as $file ){
                $file_name = Requests::cyrillic_translit( $file['name'] );

                if( move_uploaded_file( $file['tmp_name'], "$upload_dir/$file_name" ) ){
                    $done_files[] = realpath( "$upload_dir/$file_name" );
                }
            }
            //DBQuery::query("INSERT INTO requests (`mail_link`) VALUES ('$upload_dir/$file_name')");
            $data = $done_files ? array('files' => $done_files ) : array('error' => 'Ошибка загрузки файлов.');

            die( json_encode( $data ) );
        }

        /**
         * Транслитерация кирилличесских символов
         *
         * @param string $title - название загружаемого файла
         * @return string|string[]|null - транслитирированное название загружаемого файла
         */
        function cyrillic_translit( $title ){
            $iso9_table = array(
                'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Ѓ' => 'G',
                'Ґ' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Є' => 'YE',
                'Ж' => 'ZH', 'З' => 'Z', 'Ѕ' => 'Z', 'И' => 'I', 'Й' => 'J',
                'Ј' => 'J', 'І' => 'I', 'Ї' => 'YI', 'К' => 'K', 'Ќ' => 'K',
                'Л' => 'L', 'Љ' => 'L', 'М' => 'M', 'Н' => 'N', 'Њ' => 'N',
                'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
                'У' => 'U', 'Ў' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'TS',
                'Ч' => 'CH', 'Џ' => 'DH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '',
                'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA',
                'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'ѓ' => 'g',
                'ґ' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'є' => 'ye',
                'ж' => 'zh', 'з' => 'z', 'ѕ' => 'z', 'и' => 'i', 'й' => 'j',
                'ј' => 'j', 'і' => 'i', 'ї' => 'yi', 'к' => 'k', 'ќ' => 'k',
                'л' => 'l', 'љ' => 'l', 'м' => 'm', 'н' => 'n', 'њ' => 'n',
                'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
                'у' => 'u', 'ў' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'ts',
                'ч' => 'ch', 'џ' => 'dh', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => '',
                'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya'
            );

            $name = strtr( $title, $iso9_table );
            $name = preg_replace('~[^A-Za-z0-9\'_\-\.]~', '-', $name );
            $name = preg_replace('~\-+~', '-', $name ); // --- на -
            $name = preg_replace('~^-+|-+$~', '', $name ); // кил - на концах

            return $name;
        }

        /**
         * Вычисление итоговой цены, рассчитанное на основе параметров, введенных пользователем в форму создания заявки
         *
         * @param integer $square - площадь потолка, введенная пользователем в форму создания заявки
         * @param integer $lamp - количество светильников, введенное пользователем в форму создания заявки
         * @param integer $chandelier - количество люстр, введенное пользователем в форму создания заявки
         * @param integer $aqueduct - количество труб, введенное пользователем в форму создания заявки
         * @param integer $corner - количество углов, введенное пользователем в форму создания заявки
         * @param string $glance - выбрано ли значение glance в форме создания заявки 'true'/'false'
         * @param string $matte - выбрано ли значение matte в форме создания заявки 'true'/'false'
         * @return integer - итоговая цена
         */
        public static function calculate_total_price($square,$lamp,$chandelier,$aqueduct,$corner,$glance,$matte){
            $prices = Settings::select_setting(1);
            if (!$prices) {
                return false;
            }
            $glance = $glance === 'true' ? true : false;
            $matte = $matte === 'true' ? true : false;
            return $total_price = $prices[1] * (int)$square
                            * ($prices[6] * (int)$glance + $prices[7] * (int)$matte)
                            + $prices[2] * (int)$lamp
                            + $prices[3] * (int)$chandelier
                            + $prices[4] * (int)$aqueduct
                            + $prices[5] * (int)$corner;
        }

        /**
         * Удаление заявки
         *
         * @param intege $id - id удаляемой заявки
         * @return mixed - выполенение запроса DELETE к базе
         */
        public static function delete_request($id){
            return DBQuery::query("DELETE FROM requests WHERE id = $id");
        }


    }



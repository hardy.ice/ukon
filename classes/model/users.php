<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/singleton.php";

    class Users{

        /**
         * Получение всех пользователей
         *
         * @return array - массив всех пользователей
         */
        public static function select_all_users(){
            $res = DBQuery::query("SELECT `id`, `login` FROM `users`");
            if (!$res){
                $res = false;
            }
            return $res;
        }

        /**
         * Получение одного пользователя
         *
         * @param integer $id - id получаемого пользователя
         * @return array - одномерный массив с данными о пользователе
         */
        public static function select_one_user($id){
            $res = DBQuery::query("SELECT `id`, `login` FROM `users` WHERE `id` = $id");
            if (!$res){
                $res = false;
            }
            return $res;
        }

        /**
         * Добавление пользователя в базу
         *
         * @param string $login - логин нового пользователя
         * @param string $password - пароль новго пользователя
         * @return mixed - выполнение запроса INSERT к базе
         */
        public static function add_user($login,$password){
            $pass_hash = password_hash($password,PASSWORD_DEFAULT);
            return DBQuery::query("INSERT INTO `users` (`login`, `password`) VALUES ('$login', '$pass_hash')");
        }

        /**
         * Удаление пользователя
         *
         * @param integer $id - id удаляемого пользователя
         * @return mixed - выполнение запроса DELETE к базе
         */
        public static function delete_user($id){
            return DBQuery::query("DELETE FROM `users` WHERE `id` = $id");
        }

        /**
         * Обновление пользователя
         *
         * @param integer $id - id обновляемого пользователя
         * @param string $login - логин пользователя
         * @param string $password - пароль пользователя
         * @return mixed - выполнение запроса UPDATE к базе
         */
        public static function update_user($id,$login,$password){
            $pass_hash = password_hash($password,PASSWORD_DEFAULT);
            return DBQuery::query("UPDATE `users` SET `login` = '$login', `password` = '$pass_hash' WHERE `id` = $id");
        }

        /**
         * Аутентификация пользователя в системе
         *
         * @param string $login - логин, введенный в форму авторизации
         * @param string $password - пароль, введенный в форму авторизации
         * @return bool - открывает сессию для пользователя
         */
        public static function log_in($login,$password){
            $correct_user = DBQuery::query("SELECT * FROM `users` WHERE `login` = '$login'");
            if (password_verify($password,$correct_user[0][2])){
                session_start();
                $_SESSION['login'] = $login;
                return $_SESSION['auth'] = true;
            }
            return false;
        }

        /**
         * Удаляет сессию для пользователя
         */
        public static function log_out(){
            session_start();
            var_dump('qq');
            unset($_SESSION['auth']);
        }

    }

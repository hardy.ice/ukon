<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/users.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/requests.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/settings.php";

    class View
    {

        function show_all_users()
        {
            $users = Users::select_all_users();
            if ($users) {
                echo '<table class="users-table">
                            <thead>
                                <tr>
                                    <th width="80px">ID</th>
                                    <th class="text">Логин пользователя</th>
                                    <th align="center" width="171px">Действие</th>
                                </tr>
                            </thead>
                            <tbody>';
                foreach ($users as $item) {

                     echo '
                                <tr dataId="'; echo $item['0']; echo '">
                                    <td>'; echo $item['0']; echo '</td>
                                    <td class="text login">'; echo $item['1']; echo '</td>
                                    <td class="action" align="center">
                                        <span class="edit edit-user" dataId="'; echo $item['0']; echo '"><i class="far fa-edit"></i></span>
                                        <span class="delete delete-user" dataId="'; echo $item['0']; echo '"><i class="fas fa-times"></i></span>
                                    </td>
                                </tr>';
                }
                echo '</tbody>
                </table>';


            }
        }

        function show_settings()
        {
            $setting = Settings::select_setting(1);
            if ($setting) {
                    echo '  <form id="update-setting-form">
                            <div class="row justify-content-between align-items-end">
                                <div class="form-group col-12 col-sm-4">
                                    <label for="square-meter-price">Цена за кв.м. потолка:</label>
                                    <input type="text" class="form-control rounded-0" id="square-meter-price" name="square_meter_price" value="'; echo $setting['1']; echo '">
                                </div>
                                <div class="form-group col-12 col-sm-4">
                                    <label for="glance-price">Цена за глянцевую фактуру:</label>
                                    <input type="text" class="form-control rounded-0" id="glance-price" name="glance_price" value="'; echo $setting['6']; echo '">
                                </div>
                                <div class="form-group col-12 col-sm-4">
                                    <label for="matte-price">Цена за матовую фактуру:</label>
                                    <input type="text" class="form-control rounded-0" id="matte-price" name="matte_price" value="'; echo $setting['7']; echo '">
                                </div>          
                            </div>
        
                            <div class="row justify-content-between align-items-end">
                                <div class="form-group col-12 col-sm-4">
                                    <label for="lamp-price">Цена за светильник:</label>
                                    <input type="text" class="form-control rounded-0" id="lamp-price" name="lamp_price" value="'; echo $setting['2']; echo '">
                                </div>
                                <div class="form-group col-12 col-sm-4">
                                    <label for="chandelier-price">Цена за люстру:</label>
                                    <input type="text" class="form-control rounded-0" id="chandelier-price" name="chandelier_price" value="'; echo $setting['3']; echo '">
                                </div>
                                <div class="form-group col-12 col-sm-4">
                                    <label for="aqueduct-price">Цена за трубу:</label>
                                    <input type="text" class="form-control rounded-0" id="aqueduct-price" name="aqueduct_price" value="'; echo $setting['4']; echo '">
                                </div>
                            </div>
        
                            <div class="row justify-content-between align-items-end">
                                <div class="form-group col-12 col-sm-4">
                                    <label for="corner-price">Цена за угол:</label>
                                    <input type="text" class="form-control rounded-0" id="corner-price" name="corner_price"  value="'; echo $setting['5']; echo '">
                                </div>   
                                <div class="form-group col-12 col-sm-4 d-none">
                                    <label for="command">Команда:</label>
                                    <input type="text" class="form-control rounded-0" id="command" name="command"  value="update-setting">
                                </div>  
                            </div>
                            
                            <div class="color-block pt-3">
                                <div id="colors-list" class="colors-list">
                                    <span class="color-header">Варианты цветов:</span><br>';


                    $colors = $setting['8'];
                    foreach ($colors as $key => $value) {
                        echo '
                                
                                    <div class="color" dataId="'; echo $key; echo '">'; echo $value; echo '
                                        <span class="delete delete-color">
                                            <i class="fas fa-times" dataId="'; echo $key; echo '"></i>
                                        </span>
                                    </div>
                                
                                
                                
                        
                        
                        ';
                    }



            echo '
                                </div>
                            </div>
                    
                    
                    
                    
                    <div class="add">Добавить новый цвет <span class="add-color-button"><i class="fas fa-plus"></i></span></div>
                    <div class="add-color-form d-none">
                        <input type="text" class="form-control rounded-0" id="new_color" name="new_color" placeholder="Введите новый цвет">
                        <span class="add-color-button">
                            <i class="fas fa-check"></i>
                        </span>
                        <span class="cancel-button">
                            <i class="fas fa-times"></i>
                        </span>
                    </div>
                    
                    <button type="submit" class="btn btn-primary btn-lg mt-3 update-settings-button rounded-0 w-100">Сохранить изменения</button>
                </form>';
            }
        }

        function show_all_requests(){
            $requests = Requests::select_all_requests();
            if ($requests) {
                echo '<table class="requests-table">
                            <thead>
                                <tr>
                                    <th width="40px">ID</th>
                                    <th width="139px">Телефон</th>
                                    <th width="161px">Дата рождения</th>
                                    <th width="157px">Город доставки</th>
                                    <th class="text">Текст заявки</th>
                                    <th width="139px">Дата заявки</th>
                                    <th width="128px">IP</th>
                                    <th align="center" width="100px">Действие</th>
                                </tr>
                            </thead>
                            <tbody>';
                    foreach ($requests as $item) {
                        echo '<tr dataId = "'; echo $item['0']; echo '"><td>'; echo $item['0']; echo '</td>
                                <td>'; echo $item['1']; echo '</td>
                                <td>'; echo date('d.m.Y', strtotime($item['2'])); echo '</td>
                                <td>'; echo $item['3']; echo '</td>
                                <td class="text">'; echo $item['4']; echo '</td>
                                <td>'; echo date('H:i d.m.y', strtotime($item['5'])); echo '</td>
                                <td>'; echo $item['6']; echo '</td>
                                <td class="action" align="center">
                                    <span class="delete delete-request" dataId = "'; echo $item['0']; echo '"><i class="fas fa-times"></i></span>
                                </td>
                            </tr>';
                    }
                    echo '</tbody>
                        </table>';
            }
        }

        function show_color(){
            $colors = Settings::get_colors_array(1);
            foreach ($colors as $key => $value){
                echo '
                
                    <div class="form-check p-0 pb-3">
                        <input class="form-check-input d-none" type="radio" name="color" id="'; echo $key; echo '" value="'; echo $value; echo '">
                        <label class="form-check-label" for="'; echo $key; echo '">
                        '; echo $value; echo '
                        </label>
                    </div>
                
                ';
            }
        }

        function show_welcome(){
            echo '<div class="welcome">Здравствуйте, <span class="welcome-login">'; echo $_SESSION['login']; echo '</span></span>
                    <div class="exit">Выйти</div>';
        }

    }
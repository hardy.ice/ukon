<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/users.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/requests.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/settings.php";


//var_dump($_POST);
//var_dump($_FILES);
//var_dump('jjj');

    switch ($_POST["command"]){

        case "send-request":{ echo Requests::add_request($_POST["delivery-city"],
                                                            date('Y-m-d', strtotime($_POST["birth-date"])),
                                                            $_POST["phone"],
                                                            $_POST["square"],
                                                            $_POST["lamp"],
                                                            $_POST["chandelier"],
                                                            $_POST["aqueduct"],
                                                            $_POST["corner"],
                                                            $_POST["texture"],
                                                            $_POST["color"],
                                                            $_POST["total-price"],
                                                            date('Y-m-d\TH:i:s.v'),
                                                            $_SERVER['REMOTE_ADDR']);
                                ;break;}

        case "delete-request":{echo (Requests::delete_request($_POST["id"])); break;}

        case "upload-file": {echo (Requests::upload_file()); break;}

        case "calculate":{echo (Requests::calculate_total_price($_POST["square"],
            $_POST["lamp"],
            $_POST["chandelier"],
            $_POST["aqueduct"],
            $_POST["corner"],
            $_POST["glance"],
            $_POST["matte"]));
            break;}




        case "add-user":{echo (Users::add_user($_POST["login_add_user"],$_POST["password_add_user"]));
                        break;}

        case "update-user":{echo (Users::update_user($_POST["id_edit_user"],
                                                    $_POST["login_edit_user"],
                                                    $_POST["password_edit_user"]));
                        break;}

        case "delete-user":{echo (Users::delete_user($_POST["id"])); break;}

        //case "edit-user":{echo (Users::update_user($_POST["id"],$_POST["login"],$_POST["password"])); break;}





        case "add-color":{echo (Settings::add_color($_POST['new_color'])); break;}

        case "delete-color":{echo (Settings::delete_color($_POST["id"])); break;}

        case "update-setting":{echo (Settings::update_setting($_POST["square_meter_price"],
                                                              $_POST["lamp_price"],
                                                              $_POST["chandelier_price"],
                                                              $_POST["aqueduct_price"],
                                                              $_POST["corner_price"],
                                                              $_POST["glance_price"],
                                                              $_POST["matte_price"],
                                                              $_POST["new_color"]));
                                break;}




        case "auth": {echo (Users::log_in($_POST["login"], $_POST["password"])); break;}

        case "exit": {echo (Users::log_out()); break;}
    }


<?php
session_start();
if ($_SESSION['auth']==1) {
    header("Location: /admin.php");
    exit;
}
?>

<!DOCTYPE HTML>
<html lasite-headerng="en">
    <head>
        <meta charset="utf-8">
        <title>Юкон — сеть компаний</title>

        <link rel="stylesheet" href="./css/reset.css">
        <link rel="stylesheet" href="./css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/nice-select.css">
        <link rel="stylesheet" href="./css/jquery-ui.min.css">
        <link rel="stylesheet" href="./css/hamburger-animation.css">

        <link rel="stylesheet" href="./css/style.css">
        <link rel="stylesheet" href="./css/adaptive.css">
        <link href="./img/favicon.ico" rel="shortcut icon" type="image/x-icon">

        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no">

    </head>
    <body>
        <div class="auth-bg">
            <div class="auth-window">
                <div class="auth-header pb-4">
                    Авторизация
                </div>
                <div class="auth-content">
                    <span>Для входа введите логин и пароль</span>
                    <form id="auth-form">
                        <div class="form-group mt-3">
                            <label for="login">Логин:</label>
                            <input type="text" class="form-control rounded-0" id="login" name="login" placeholder="Ваш логин">
                        </div>
                        <div class="form-group mt-3">
                            <label for="password">Пароль:</label>
                            <input type="password" class="form-control rounded-0" id="password" name="password" placeholder="Ваш пароль">
                        </div>
                        <div class="form-group d-none">
                            <label for="command">Команда:</label>
                            <input type="text" class="form-control rounded-0 d-none" id="command" name="command" value="auth">
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg mt-3 auth-button rounded-0 w-100 uppercase">Войти</button>
                    </form>
                </div>
            </div>
        </div>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="./js/jquery.js"></script>
        <script src="./js/jquery-ui.js"></script>
        <script src="./js/jquery.maskedinput.min.js"></script>
        <script src="./js/jquery.nice-select.js"></script>
        <script src="./js/datepicker-ru.js"></script>
        <script src="./js/jquery.validate.min.js"></script>
        <script src="./js/script.js"></script>
    </body>
</html>
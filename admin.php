<?php
    session_start();
    if (empty($_SESSION['auth'])) {
        header("Location: /auth.php");
    }
?>


<!DOCTYPE HTML>
<html lasite-headerng="en">
    <head>
        <meta charset="utf-8">
        <title>Юкон — сеть компаний</title>

        <link rel="stylesheet" href="./css/reset.css">
        <link rel="stylesheet" href="./css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/nice-select.css">
        <link rel="stylesheet" href="./css/jquery-ui.min.css">
        <link rel="stylesheet" href="./css/hamburger-animation.css">

        <link rel="stylesheet" href="./css/style.css">
        <link rel="stylesheet" href="./css/adaptive.css">
        <link href="./img/favicon.ico" rel="shortcut icon" type="image/x-icon">

        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no">

    </head>
    <body>
        <div class="mt-5 mt-md-0">
            <header class="d-none d-md-block">
                <div class="site-header">
                    <div class="container wrap pl-0 pr-0">
                        <div class="row m-0 justify-content-between align-items-center">
                            <div class="col-2 col-md-3 logo">
                                <a href="#"><img src="./img/header-logo.png"></a>
                            </div>


                            <div class="col-8 col-md-5 col-lg-6 h-100 info">
                                <div class="row pl-3">
                                    <div class="col-1 icon d-none d-xl-block order-xl-1"><i class="fas fa-map-marker-alt"></i></div>
                                    <div class="col-12 col-xl-6 pl-xl-0 order-1 order-xl-2 no-wrap">г. Ростов-на-Дону, Шаумяна, 73</div>
                                    <div class="col-1 icon d-none d-xl-block order-xl-3"><i class="fas fa-phone"></i></div>
                                    <div class="col-12 col-xl-4 pl-xl-0 phone-number order-2 order-xl-4"><a href="tel:+78632298182">+7 (863) 229-81-82</a></div>
                                
                                    <div class="col-1 icon order-xl-5 d-none d-xl-flex" style="color:transparent;"><i class="fas fa-map-marker-alt"></i></div>
                                    <div class="col-12 col-xl-6 pl-xl-0 order-3 order-xl-6 no-wrap">г. Волгодонск, ул. Энтузиастов, 13</div>
                                    <div class="col-1 icon order-xl-7 d-none d-xl-flex" style="color:transparent;"><i class="fas fa-phone"></i></div>
                                    <div class="col-12 col-xl-4 pl-xl-0 phone-number order-4 order-xl-8"><a href="tel:+78639247979">+7 (8639) 24-79-79</a></div>
                                </div>
                            </div>
                            <div class="col-2 col-md-4 col-lg-3 justify-content-end">
                                <?php require_once $_SERVER["DOCUMENT_ROOT"]."/classes/view/view.php";
                                View::show_welcome();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main>
                <div class="wrap">







                    <section>
                        <div class="section-header">
                            Пользователи
                        </div>


                        <?php
                            View::show_all_users();
                            ?>


                        <div class="section-subheader">
                            Добавление нового пользователя
                        </div>
                        <form id="add-user-form" class="">
                            <div class="form-group mt-3">
                                <label for="login">Логин:</label>
                                <input type="text" class="form-control rounded-0" id="login_add_user" name="login_add_user" placeholder="Логин пользователя">
                            </div>
                            <div class="form-group mt-3">
                                <label for="password">Пароль:</label>
                                <input type="text" class="form-control rounded-0" id="password_add_user" name="password_add_user" placeholder="Пароль пользователя">
                            </div>
                            <div class="form-group d-none">
                                <label for="command">Команда:</label>
                                <input type="text" class="form-control rounded-0 d-none" id="command" name="command" value="add-user">
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg mt-3 add-user-button rounded-0 w-100 uppercase">Добавить пользователя</button>
                        </form>
                    </section>





                    <section class="calc-settings">

                        <div class="section-header">
                            Настройки калькулятора
                        </div>

                        <?php
                        View::show_settings();
                        ?>



                    </section>






                    <section>
                        <div class="section-header">
                            Заявки
                        </div>


                        <?php
                        View::show_all_requests();
                        ?>


                    </section>
                </div>
            </main>
        </div>



        <div class="modal-bg modal-message hidden">
            <div class="modal-window p-3">
                <div class="modal-header">
                    Действие выполнено
                </div>
                <div class="close-button">
                </div>
                <div class="modal-text">
                    Действие выполнено.
                </div>
            </div>
        </div>


        <div class="modal-bg modal-edit-user hidden">
            <div class="modal-window p-3">
                <div class="modal-header">
                    Редактирование пользователя
                </div>
                <div class="close-button">
                </div>
                <div class="modal-text">
                    <form id="edit-user-form" class="">
                        <div class="form-group mt-3">
                            <label for="id_edit_user">ID:</label>
                            <input type="text" class="form-control rounded-0" id="id_edit_user" name="id_edit_user" readonly>
                        </div>
                        <div class="form-group mt-3">
                            <label for="login_edit_user">Логин:</label>
                            <input type="text" class="form-control rounded-0" id="login_edit_user" name="login_edit_user" readonly>
                        </div>
                        <div class="form-group mt-3 with-hint">
                            <label for="password_edit_user">Пароль:</label>
                            <input type="text" class="form-control rounded-0" id="password_edit_user" name="password_edit_user" placeholder="Пароль пользователя">
                            <div class="">
                                <span class="hint-i"><i class="far fa-question-circle"></i></span>
                                <div class="hint-text">Если Вы оставите пароль пустым, то для пользователя останется его предыдущий пароль</div>
                            </div>
                        </div>
                        <div class="form-group d-none">
                            <label for="command">Команда:</label>
                            <input type="text" class="form-control rounded-0 d-none" id="command" name="command" value="update-user">
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg mt-3 edit-user-button rounded-0 w-100 uppercase">Редактировать пользователя</button>
                    </form>
                </div>
            </div>
        </div>



        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="./js/jquery.js"></script>
        <script src="./js/jquery-ui.js"></script>
        <script src="./js/jquery.maskedinput.min.js"></script>
        <script src="./js/jquery.nice-select.js"></script>
        <script src="./js/datepicker-ru.js"></script>
        <script src="./js/jquery.validate.min.js"></script>
        <script src="./js/script.js"></script>
    </body>
</html>
<!DOCTYPE HTML>
<html lasite-headerng="en">
	<head>
		<meta charset="utf-8">
		<title>Юкон — сеть компаний</title>

		<link rel="stylesheet" href="./css/reset.css">
		<link rel="stylesheet" href="./css/bootstrap.min.css">
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
		<link rel="stylesheet" href="./css/nice-select.css">
		<link rel="stylesheet" href="./css/jquery-ui.min.css">
		<link rel="stylesheet" href="./css/hamburger-animation.css">
	
		<link rel="stylesheet" href="./css/style.css">
		<link rel="stylesheet" href="./css/adaptive.css">
		<link href="./img/favicon.ico" rel="shortcut icon" type="image/x-icon">

		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no">

	</head>
	<body>


		<div class="mt-5 mt-md-0">
			<header class="d-none d-md-block">
				<div class="site-header">
					<div class="container wrap pl-0 pr-0">
						<div class="row m-0 justify-content-between align-items-center">
							<div class="col-2 col-md-3 logo">
								<a href="#"><img src="./img/header-logo.png" alt="Ukon-logo"></a>
							</div>


							<div class="col-8 col-md-5 col-lg-6 h-100 info">
								<div class="row pl-3">
									<div class="col-1 icon d-none d-xl-block order-xl-1"><i class="fas fa-map-marker-alt"></i></div>
									<div class="col-12 col-xl-6 pl-xl-0 order-1 order-xl-2 no-wrap">г. Ростов-на-Дону, Шаумяна, 73</div>
									<div class="col-1 icon d-none d-xl-block order-xl-3"><i class="fas fa-phone"></i></div>
									<div class="col-12 col-xl-4 pl-xl-0 phone-number order-2 order-xl-4"><a href="tel:+78632298182">+7 (863) 229-81-82</a></div>
								
									<div class="col-1 icon order-xl-5 d-none d-xl-flex" style="color:transparent;"><i class="fas fa-map-marker-alt"></i></div>
									<div class="col-12 col-xl-6 pl-xl-0 order-3 order-xl-6 no-wrap">г. Волгодонск, ул. Энтузиастов, 13</div>
									<div class="col-1 icon order-xl-7 d-none d-xl-flex" style="color:transparent;"><i class="fas fa-phone"></i></div>
									<div class="col-12 col-xl-4 pl-xl-0 phone-number order-4 order-xl-8"><a href="tel:+78639247979">+7 (8639) 24-79-79</a></div>
								</div>
							</div>
							<div class="col-2 col-md-4 col-lg-3 justify-content-end">
								<a class="calc-button p-3" href="#">
									<span>
										<i class="fas fa-lg mr-1 fa-calculator"></i>
										<span class="d-none d-md-inline">
											Калькулятор
											<span class="d-none d-xl-inline"> онлайн</span>
										</span>
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>

				<nav class="main-nav">
					<div class="container wrap pl-0 pr-0" role="navigation">
						<ul class="nav justify-content-between">
							<li class="nav-item"><a class="nav-link active" href="#">О нас</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Каталог</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Услуги</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Цены</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Конструкции</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Акции</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Наши работы</a></li>
							<li class="nav-item"><a class="nav-link" href="#">Контакты</a></li>
						</ul>
					</div>
				</nav>	
			</header>






			<main>
				<div class="container content wrap  ml-3 ml-xl-auto mr-3 mr-xl-auto">
					<form class="w-100" id="request-form" enctype="multipart/form-data">
						<div class="row">

								<div class="col-12 col-lg-5 pt-4 pt-sm-5 order-12 order-lg-1 request">
									<h2 class="uppercase"><b>Итоговая цена:</b></h2>
                                    <div class="total-price"><h2><span class="total-price-border"><b><span class="calculated-total-price">0</span> рублей</b></span></h2></div>
									<div class="form-group pt-3">
										<label for="delivery-city">Город доставки:</label>
										<select class="form-control rounded-0" id="delivery-city" name="delivery-city">
											<option data-display="Выберите город"></option>
											<option value="moscow">Москва</option>
											<option value="st-petersburg">Санкт-Петербург</option>
											<option value="rostov-on-don">Ростов-на-Дону</option>
											<option value="ufa">Уфа</option>
											<option value="vladivostok">Владивосток</option>
										</select>
									</div>
									<div class="form-group">
										<label for="birth-date">Дата рождения:</label>
										<div class="d-flex">
											<input class="form-control rounded-0" id="birth-date" name="birth-date">
										</div>
									</div>
									<div class="form-group pos">
										<label for="phone">Телефон:</label>
										<input type="tel" class="form-control rounded-0 phone" id="phone" name="phone" placeholder="+7 (___) ___-__-__">
									</div>
									<div class="form-group">
										<div class="like-label">
											Письменная заявка:
										</div>
										<div class="file-wrapper d-flex">
											<span class="filupp-file-name js-value">Загрузить файл</span>
											<input type="file" class="form-control rounded-0" id="application" name="application">

											<label for="application" class="file-label">
												<i class="fas fa-search"></i>
											</label>

										</div>
									</div>
									<div class="form-group pos d-none">
										<label for="command">Команда:</label>
										<input type="text" class="form-control rounded-0" id="command" name="command" value="send-request">
									</div>
									<button type="submit" class="btn btn-primary btn-lg mt-3 send-request rounded-0 w-100 uppercase">Оставить заявку</button>
									<div class="form-check pt-3 checkbox-div">
										<input type="checkbox" class="form-check-input w-0 personal-data" id="personal-data" name="personaldata">
										<label class="form-check-label personal-data-label" for="personal-data">Я согласен на <a href="#">обработку персональных данных</a></label>
									</div>
								</div>



								<div class="col-12 col-lg-7 p-4 p-sm-5 pl-md-4 pr-md-4 p-xl-5 order-1 order-lg-12 calc-form">
									<h2 class="mb-5"><b><i class="fas fa-calculator mr-2"></i>Калькулятор расчета стоимости</b></h2>
									<div class="row">
										<div class="col-12 col-md-7 first-col">
											<fieldset class="form-group">
												<legend class="uppercase"><b>Характеристики:</b></legend>			
												<div class="form-group row">
													<label for="square" class="col-6 col-sm-7 col-form-label">Площадь потолка:</label>
													<div class="col-6 col-sm-5">
														<div class="row">
															<input type="text" class="col form-control rounded-0" id="square" name="square" placeholder="0" maxlength="4">
															<div class="col form-text">кв.м</div>
														</div>
													</div>
												</div>
											</fieldset>
											<fieldset class="form-group">
												<legend>Количество</legend>	
													<div class="form-group row justify-content-end nowrap">
														<label for="lamp" class="col-6 col-form-label pl-4">светильников:</label>
														<div class="col-6 col-sm-5">
															<div class="row">
																<input type="text" class="col-6 form-control rounded-0" id="lamp" name="lamp" placeholder="0" maxlength="4">
																<div class="col-6 form-text">шт</div>
															</div>
														</div>
													</div>
													<div class="form-group row justify-content-end">
														<label for="chandelier" class="col-6 col-form-label pl-4">люстр:</label>
														<div class="col-6 col-sm-5">
															<div class="row">
																<input type="text" class="col-2  form-control rounded-0" id="chandelier" name="chandelier" placeholder="0" maxlength="4">
																<div class="col-1 form-text">шт</div>
															</div>
														</div>
													</div>
													<div class="form-group row justify-content-end">
														<label for="aqueduct" class="col-6 col-form-label pl-4">труб:</label>
														<div class="col-6 col-sm-5">
															<div class="row">
																<input type="text" class="col-2 form-control rounded-0" id="aqueduct" name="aqueduct" placeholder="0" maxlength="4">
																<div class="col-1 form-text">шт</div>
															</div>
														</div>
													</div>
													<div class="form-group row justify-content-end">
														<label for="corner" class="col-6 col-form-label pl-4">углов:</label>
														<div class="col-6 col-sm-5">
															<div class="row">
																<input type="text" class="col-2 form-control rounded-0" id="corner" name="corner" placeholder="0" maxlength="4">
																<div class="col-1 form-text">шт</div>
															</div>
														</div>
													</div>
												</fieldset>
										</div>
										<div class="col-12 col-md-5 second-col pl-3 pl-md-4">
					  						<fieldset class="form-group">
					  							<legend class="uppercase"><b>Фактура:</b></legend>
					  							<div class="form-check p-0 pb-3">
													<input class="form-check-input d-none" type="radio" name="texture" id="glance" value="glance" checked>
													<label class="form-check-label" for="glance">
													глянцевая
													</label>
												</div>
												<div class="form-check p-0 pb-3">
													<input class="form-check-input d-none" type="radio" name="texture" id="matte" value="matte">
													<label class="form-check-label" for="matte">
													матовая
													</label>
												</div>
					  						</fieldset>
					  						<fieldset class="form-group colors-form-group">
					  							<legend class="uppercase"><b>Цвет:</b></legend>

                                                <?php require_once $_SERVER["DOCUMENT_ROOT"]."/classes/view/view.php";
                                                View::show_color();
                                                ?>
					  						</fieldset>
										</div>
									</div>
								</div>
						</div>
					</form>
				</div>
			</main>







			<footer>
				<div class="links">
					<div class="footer-container">
						<div class="row pt-4 pb-5 m-0">
							<div class="col-12 col-lg links-column">
								<div class="footer-list-btn">
									<div class="h5"><b>Компания</b></div>
									<span class="arrow arrow-closed"></span>
									<ul class="footer-list company-height footer-list-closed">
										<li><a href="#">О нас</a></li>
										<li><a href="#">Цены</a></li>
										<li><a href="#">Акции</a></li>
										<li><a href="#">Наши работы</a></li>
										<li><a href="#">Гарантия</a></li>
										<li><a href="#">Калькулятор</a></li>
									</ul>
								</div>
							</div>
							<div class="col-12 col-lg links-column">
								<div class="footer-list-btn">
									<div class="h5"><b>Каталог потолков</b></div>
									<span class="arrow arrow-closed"></span>
									<ul class="footer-list catalog-height footer-list-closed">
										<li><a href="#">Глянцевые</a></li>
										<li><a href="#">Матовые</a></li>
										<li><a href="#">Сатиновые</a></li>
										<li><a href="#">С фотопечатью</a></li>
										<li><a href="#">Звездное небо</a></li>
										<li><a href="#">Художественные</a></li>
									</ul>
								</div>
							</div>
							<div class="col-12 col-lg  links-column">
								<div class="footer-list-btn">
									<div class="h5"><b>Услуги</b></div>
									<span class="arrow arrow-closed"></span>
									<ul class="footer-list services-height footer-list-closed">
										<li><a href="#">Установка натяжных потолков</a></li>
										<li><a href="#">Двухуровневые потолки</a></li>
										<li><a href="#">Потолки с подвеской</a></li>
										<li><a href="#">Ремонт потолков</a></li>
									</ul>
								</div>
							</div>
							<div class="col-12 col-lg links-column">
								<div class="footer-list-btn">
									<div class="h5"><b>Конструкции</b></div>
									<span class="arrow arrow-closed"></span>
									<ul class="footer-list constructions-height footer-list-closed">
										<li><a href="#">Одноуровневые</a></li>
										<li><a href="#">Двухуровневые</a></li>
										<li><a href="#">Многоуровневые</a></li>
									</ul>
								</div>
							</div>
							<div class="d-none d-lg-block col-4 info">
								<a href="#">
									<img src="./img/footer-logo.png" alt="Ukon-logo">
									<h1 class="uppercase"><i><b>Юкон</b></i></h1>
									<div class="h5"><b>натяжные потолки</b></div>
								</a>
								<div class="info-block">
									<b>г. Ростов-на-Дону, Шаумяна, 73</b>
									<br>
									<b><a href="tel:+78632298182">+7 (863) 229-81-82</a></b>
								</div>
								<div class="info-block">
									<b>г. Волгодонск, ул. Энтузиастов, 13</b>
									<br>
									<b><a href="tel:+78639247979">+7 (8639) 24-79-79</a></b>
								</div>
							</div>
							<div class="d-block d-lg-none col-12 info links-column">
								<div class="footer-list-btn">
									<div class="h5"><b>Контакты</b></div>
									<span class="arrow arrow-closed"></span>
									<div class="info-height footer-list footer-list-closed">
										<div class="info-block">
											г. Ростов-на-Дону, Шаумяна, 73
											<br>
											<a href="tel:+78632298182">+7 (863) 229-81-82</a>
										</div>
										<div class="info-block">
											г. Волгодонск, ул. Энтузиастов, 13
											<br>
											<a href="tel:+78639247979">+7 (8639) 24-79-79</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="bottom">
					<div class="footer-container bottom-container">
						<div class="row m-0 justify-content-between">
							<div class="col-12 col-sm">ООО "ИТ-Групп"</div>
							<div class="col-12 col-sm col-md-4"><a href="https://firecode.ru/" target="_blank">Создание сайта - <span class="uppercase">FIRECODE</span></a></div>
						</div>
					</div>
				</div>
			</footer>
		</div>






		<nav>
			<div class="navbar fixed-top navbar-expand-md d-md-none p-0">
				<div class="container wrap pl-0 pr-0" role="navigation">
					<a class="navbar-brand d-md-none" href="#"><img src="./img/header-logo.png" alt="Ukon-logo"></a>
					<div class="navbar-toggler p-0" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon">
							<div class="icon nav-icon-5">
								<span></span>
								<span></span>
								<span></span>
							</div>
						</span>
					</div>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav justify-content-between">
							<li class="nav-item">
								<a class="nav-link active" href="#">О нас<span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Каталог</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Услуги</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Цены</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Конструкции</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Акции</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Наши работы</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Контакты</a>
							</li>
							<hr>
							<li>
								<span class="like-nav-link">г. Ростов-на-Дону, Шаумяна, 73</span>
							</li>
							<li>
								<a class="nav-link" href="tel:+78632298182">+7 (863) 229-81-82</a>
							</li>
							<hr>
							<li>
								<span class="like-nav-link">г. Волгодонск, ул. Энтузиастов, 13</span>
							</li>
							<li>
								<a class="nav-link" href="tel:+78639247979">+7 (8639) 24-79-79</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>


		<div class="modal-bg hidden">
			<div class="modal-window p-3">
				<div class="modal-header">
					Спасибо за заявку!
				</div>
				<div class="close-button">
				</div>
				<div class="modal-text">
					Спасибо за Вашу заявку. Наш менеджер свяжется с Вами в ближайшее время!
				</div>
			</div>
		</div>



		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="./js/jquery.js"></script>
		<script src="./js/jquery-ui.js"></script>
		<script src="./js/jquery.maskedinput.min.js"></script>
		<script src="./js/jquery.nice-select.js"></script>
		<script src="./js/datepicker-ru.js"></script>
		<script src="./js/jquery.validate.min.js"></script>
		<script src="./js/script.js"></script>
	</body>
</html>
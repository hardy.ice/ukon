$(document).ready(function() {



//datepicker	
	$('#birth-date').datepicker({
		showOtherMonths:true,
		selectOtherMonths:true,
		showOn:"button",
		buttonText: "<i class='fas fa-lg fa-calculator'></i>"
	});

	$.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );


//добавление маски для поля Номер телефона
	$("#phone").mask("+7 (999) 999-99-99");

//добавление атрибута checked первому цвету в калькуляторе
	$("fieldset.colors-form-group > legend + div.form-check > input").attr('checked',true);

//добавление Nice Select в поле Дата рождения
	$('#delivery-city').niceSelect();


//добавление класса open иконке бутерброда
	const icons = document.querySelectorAll('.icon');
	icons.forEach (icon => {  
		icon.addEventListener('click', (event) => {
			icon.classList.toggle("open");
		});
	});


//смена класса у групп ссылок в футере на маленьких экранах
	$(".footer-list-btn").on("click",function() {
		$(this).find(".footer-list").toggleClass("footer-list-closed").toggleClass("footer-list-opened");
		$(this).find(".arrow").toggleClass("arrow-closed").toggleClass("arrow-opened");
	});




//функция загрузки файла
	function btnComm(command)          
    {
	    var fmData =  new FormData(document.getElementById('application'));
	    $.each( $('input#application').prop('files'), function( key, value ){
		  fmData.append( key, value );
		});
        fmData.append('command','upload-file');

        $.ajax({
             url: './classes/controller/controller.php',
             data: fmData,
             cache:false,
             processData: false,
             contentType: false,
             type: 'POST',
             success: function(respond){
             		//alert('файл загружен'); 
             		console.log(respond)},
             error: function(){alert('файл не загружен')}
        });   
    }

//валидация формы добавления заявки
	$("#request-form").validate({
			rules: {
				square: {
					digits: true,
					maxlength: 4
				},
				lamp: {
					digits: true,
					maxlength: 4
				},
				chandelier: {
					digits: true,
					maxlength: 4
				},
				aqueduct: {
					digits: true,
					maxlength: 4
				},
				corner: {
					digits: true,
					maxlength: 4
				},
				phone: {
					required: true
				},
				personaldata:{
					required: true
				}
			},
			messages: {
				square: {
					digits: "Введите цифры!",
					maxlength: "Максимальное значение должно быть не больше 4х символов."
				},
				lamp: {
					digits: "Введите цифры!",
					maxlength: "Максимальное значение должно быть не больше 4х символов."
				},
				chandelier: {
					digits: "Введите цифры!",
					maxlength: "Максимальное значение должно быть не больше 4х символов."
				},
				aqueduct: {
					digits: "Введите цифры!",
					maxlength: "Максимальное значение должно быть не больше 4х символов."
				},
				corner: {
					digits: "Введите цифры!",
					maxlength: "Максимальное значение должно быть не больше 4х символов."
				},
				phone: {
					required: "Поле обязательно для заполнения!"
				},
				personaldata:{
					required: "Поле обязательно для заполнения!"
				}			
			},
			errorElement: "div",
			errorClass: "error d-inline",
			validClass: "valid",
			submitHandler: function (form) {
				var form_serialized = $(form).serializeArray();
				form_serialized.push({name:'total-price',value:$('span.calculated-total-price').val()});
				btnComm('upload-file');
				$.post("classes/controller/controller.php", form_serialized)
					.done (function (result) {
						$(".modal-bg").removeClass("hidden");
						$('#request-form')[0].reset();
						$('.js-value').text('Загрузить файл');
					})
					.fail (function () {
						alert("Ошибка доступа к серверу");
					});
			},
			invalidHandler: function (event, validator) {
				alert("Проверьте корректность заполнения формы!");
			}
	});

//удаление заявки
	$(".delete.delete-request").on("click",function(){
		var comm=[];
		comm.push({name:"command",value:"delete-request"});
		comm.push({name:"id",value: $(this).attr("dataId")});
		var dataId = $(this).attr("dataId");
		console.log(data)
		$.post("classes/controller/controller.php", comm)
			.done (function (result) {
				console.log(result);
				if (result){
					$('tr[dataId="'+dataId+'"]').remove();
					$(".modal-bg.modal-message").removeClass("hidden");
				}
				else{
					alert ("Действие не выполнено.")
				}
			})
			.fail (function () {
				alert("Ошибка доступа к серверу");
			});
	});





//валидация формы добавления пользователя
	$("#add-user-form").validate({
			rules: {
				login: {
					required: true
				},
				password: {
					required: true,
					minlength: 4
				}
			},
			messages: {
				login: {
					required: "Поле обязательно для заполнения!"
				},
				password: {
					required: "Поле обязательно для заполнения!",
					minlength: "Минимальное значение должно быть не меньше 4х символов."
				}			
			},
			errorElement: "div",
			errorClass: "error",
			validClass: "valid",
			submitHandler: function (form) {
				$.post("classes/controller/controller.php", $(form).serialize())
					.done (function (result) {
						console.log(result);
						if (result){
							$(".modal-bg.modal-message").removeClass("hidden");
							$('#add-user-form')[0].reset();
						}
						else{
							alert ("Действие не выполнено. Пользователь с таким логином уже существует.")
						}
					})
					.fail (function () {
						alert("Ошибка доступа к серверу");
					});
			},
			invalidHandler: function (event, validator) {
				alert("Проверьте корректность заполнения формы!");
			}
	});

//открытие формы редактирования пользователя
	$(".edit.edit-user").on("click",function(){
		$("#id_edit_user").val($(this).attr("dataId"));
		$("#login_edit_user").val($(this).parent().parent().find(".login").html());
		$(".modal-bg.modal-edit-user").removeClass("hidden");		
	});

//валидация формы редактирования пользователя
	$("#edit-user-form").validate({
			rules: {
				id_edit_user: {
					required: true
				},
				login_edit_user: {
					required: true,
				}
			},
			messages: {
				id_edit_user: {
					required: "Поле обязательно для заполнения!"
				},
				login_edit_user: {
					required: "Поле обязательно для заполнения!"
				}		
			},
			errorElement: "div",
			errorClass: "error",
			validClass: "valid",
			submitHandler: function (form) {
				$.post("classes/controller/controller.php", $(form).serialize())
					.done (function (result) {
						console.log(result);
						if (result){
							$(".modal-bg.modal-edit-user").addClass("hidden");
							$(".modal-bg.modal-message").removeClass("hidden");
						}
						else{
							alert ("Действие не выполнено.")
						}
					})
					.fail (function () {
						alert("Ошибка доступа к серверу");
					});
			},
			invalidHandler: function (event, validator) {
				alert("Проверьте корректность заполнения формы!");
			}
	
	});

//удаление пользователя
	$(".delete.delete-user").on("click",function(){
		var comm=[];
		comm.push({name:"command",value:"delete-user"});
		comm.push({name:"id",value: $(this).attr("dataId")});
		var dataId = $(this).attr("dataId");
		$.post("classes/controller/controller.php", comm)
			.done (function (result){
				if (result){
					$('tr[dataId="'+dataId+'"]').remove();
					$(".modal-bg.modal-message").removeClass("hidden");
				}
				else{
					alert ("Действие не выполнено.")
				}
			})
			.fail (function() {
				alert("Ошибка доступа к серверу");
			});
	});




//валидация формы изменения настроек
	$("#update-setting-form").validate({
			rules: {
				square_meter_price: {
					digits: true,
					required: true
				},
				glance_price: {
					digits: true,
					required: true,
				},
				matte_price: {
					digits: true,
					required: true,
				},
				lamp_price: {
					digits: true,
					required: true,
				},
				chandelier_price: {
					digits: true,
					required: true,
				},
				aqueduct_price: {
					digits: true,
					required: true,
				},
				corner_price: {
					digits: true,
					required: true,
				}
			},
			messages: {
				square_meter_price: {
					digits: "Введите цифры!",
					required: "Поле обязательно для заполнения!"
				},
				glance_price: {
					digits: "Введите цифры!",
					required: "Поле обязательно для заполнения!"
				},
				matte_price: {
					digits: "Введите цифры!",
					required: "Поле обязательно для заполнения!"
				},
				lamp_price: {
					digits: "Введите цифры!",
					required: "Поле обязательно для заполнения!"
				},
				chandelier_price: {
					digits: "Введите цифры!",
					required: "Поле обязательно для заполнения!"
				},
				aqueduct_price: {
					digits: "Введите цифры!",
					required: "Поле обязательно для заполнения!"
				},
				corner_price: {
					digits: "Введите цифры!",
					required: "Поле обязательно для заполнения!"
				}			
			},
			errorElement: "div",
			errorClass: "error",
			validClass: "valid",
			submitHandler: function (form) {
				$.post("classes/controller/controller.php", $(form).serialize())
					.done (function (result) {
						//console.log($(form));
						if (result){
							console.log(result);
							$(".modal-bg.modal-message").removeClass("hidden");
						}
						else{
							alert ("Действие не выполнено.")
						}
					})
					.fail (function () {
						alert("Ошибка доступа к серверу");
					});
			},
			invalidHandler: function (event, validator) {
				alert("Проверьте корректность заполнения формы!");
			}
	});

//открытие поля добавления цвета
	$("span.add-color-button > i.fa-plus").on("click",function(){
		$("div.add-color-form").removeClass("d-none");
		$("div.add").addClass("d-none");
	});

//добавление нового цвета
	$("span.add-color-button > i.fa-check").on("click",function(){
		var comm = [];
		comm.push({name:"new_color",value:$(this).parent().parent().find("#new_color").val()});
		comm.push({name:"command",value:"add-color"});
		var newColor = document.createElement('div');
		var newId = Number($('#colors-list div').last().attr("dataId")) + 1;
		newColor.innerHTML = $(this).parent().parent().find("#new_color").val()+' <span class="delete delete-color"><i class="fas fa-times" dataId="'+newId+'"></i></span>';
		newColor.setAttribute("class",'color');
		newColor.setAttribute("dataId",newId);

		$.post("classes/controller/controller.php", comm)
			.done (function (result){
				console.log(result);
				if (result){
					document.getElementById('colors-list').appendChild(newColor);
					$(".modal-bg.modal-message").removeClass("hidden");
					$('input#new-color').val("");
					$("div.add-color-form").addClass("d-none");
					$("div.add").removeClass("d-none");
				}
				else{
					alert ("Действие не выполнено.")
				}
			})
			.fail (function() {
				alert("Ошибка доступа к серверу");
			});
	});

//отмена добавления новго цвета
	$("span.cancel-button > i.fa-times").on("click",function(){
		$('input#new-color').val("");
		$("div.add-color-form").addClass("d-none");
		$("div.add").removeClass("d-none");
	});

//удаление цвета
	$("span.delete-color > i.fa-times").on("click",function(){
		var comm=[];
		comm.push({name:"command",value:"delete-color"});
		comm.push({name:"id",value: $(this).attr("dataId")});
		var dataId = $(this).attr("dataId");
		console.log(dataId);
		$.post("classes/controller/controller.php", comm)
			.done (function (result){
				console.log(result);
				if (result){
					$('div.color[dataId="'+dataId+'"]').remove();
					$(".modal-bg.modal-message").removeClass("hidden");
				}
				else{
					alert ("Действие не выполнено.")
				}
			})
			.fail (function() {
				alert("Ошибка доступа к серверу");
			});
	});



//калькулятор
	$("div.calc-form input").on("input",function(){
		var post_data=[];
		post_data.push({name:"square",value:$("#square").val()});
		post_data.push({name:"lamp",value:$("#lamp").val()});
		post_data.push({name:"chandelier",value:$("#chandelier").val()});
		post_data.push({name:"aqueduct",value:$("#aqueduct").val()});
		post_data.push({name:"corner",value:$("#corner").val()});
		post_data.push({name:"glance",value:$("#glance").prop("checked")});
		post_data.push({name:"matte",value:$("#matte").prop("checked")});
		post_data.push({name:"command",value:"calculate"})
		//console.log($(post_data));

		$.post("classes/controller/controller.php", post_data)
			.done (function (result){
				if (result){
					var str = result;
					str = str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
					$("span.calculated-total-price").html(str);
				}
				else{
					alert ("Действие не выполнено.")
				}
			})
			.fail (function() {
				alert("Ошибка доступа к серверу");
			});

	});



//вставка названия загруженного файла в поле "Письменная заявка"
	$('input[type="file"]').on("change",function(){
		var value = $("input[type='file']").val();
		var splittedFakePath = this.value.split('\\');
		$('.js-value').text(splittedFakePath[splittedFakePath.length - 1]);
	});




//смена класса модального окна при нажатии на крестик
	$(".close-button").on("click",function() {
		$(".modal-bg").addClass("hidden");
	});




//авторизация
	$("#auth-form").validate({
			rules: {
				login: {
					required: true
				},
				password: {
					required: true
				}
			},
			messages: {
				login: {
					required: "Поле обязательно для заполнения!"
				},
				password: {
					required: "Поле обязательно для заполнения!"
				}			
			},
			errorElement: "div",
			errorClass: "error",
			validClass: "valid",
			submitHandler: function (form) {
				$.post("classes/controller/controller.php", $(form).serialize())
					.done (function (result) {
						//console.log($(form));
						if (result){
							console.log(result);
							document.location.href = 'http://ukon/admin.php';
						}
						else{
							console.log(result);
							alert ("Проверьте введенные данные.")
						}
					})
					.fail (function () {
						alert("Ошибка доступа к серверу");
					});
			},
			invalidHandler: function (event, validator) {
				alert("Проверьте корректность заполнения формы!");
			}
	});

//выход
	$("div.exit").on("click",function() {
		var post_data=[];
		post_data.push({name:"command",value:'exit'});
		$.post("classes/controller/controller.php", post_data)
			.done (function (result){
				if (result){
					document.location.href = 'http://ukon/auth.php';
				}
				else{
					console.log(result);
					alert ("Действие не выполнено.")
				}
			})
			.fail (function() {
				alert("Ошибка доступа к серверу");
			});
	});

});